#' @title Coverage of the package
#' @concept Job
#' @description Compute the tests coverage
#' @param useCiOptions 1 (default) to use CI options as defined in optionsForCi.R,
#'                     other values to not use them
#' @param repo The repository used to download packages
#' @param errorExpected 0 (default) to expect this function run fine,
#'                      other values to expect an error (useful for tests)
#' @return NULL
#' @examples
#' \dontrun{
#'     citoolsr::testsCoverage()
#' }
#' @export
testsCoverage <- function(useCiOptions = 1,
                          repo = "https://cloud.r-project.org",
                          errorExpected = 0) {
    if (useCiOptions == 1) {
        beforeOptions <- citoolsr::setOptionsForCi()
    }

    citoolsr::installPackageIfNeeded(
        packageToInstallName = "covr",
        doUpdate = 0,
        repo = repo,
        useCiOptions = 0
    )

    coverageDir <- "./coverage"
    if (!dir.exists(coverageDir)) {
        dir.create(coverageDir, recursive = TRUE)
    }

    cov <- tryCatch(
        expr = {
            covr::package_coverage()
        },
            error = function(e) {
            return(FALSE)
        })

    print(cov)

    if (is.logical(cov) && cov == FALSE) {
        if (errorExpected != 1) {
            stop("Error but none expected!")
        }
    } else {
        if (errorExpected == 1) {
            stop("No error but one is expected!")
        }

        citoolsr::installPackageIfNeeded(
            packageToInstallName = "DT",
            doUpdate = 0,
            repo = repo,
            useCiOptions = 0
        )

        citoolsr::installPackageIfNeeded(
            packageToInstallName = "htmltools",
            doUpdate = 0,
            repo = repo,
            useCiOptions = 0
        )

        print("Generate covr report...")
        filename <- file.path("coverage", "coverage-report.html")
        res <- covr::report(x = cov, file = filename, browse = FALSE)
        print(res)
        if (!file.exists(filename)) {
            stop("File \"", filename, "\" not found!")
        }

        print("Generate cobertura report...")
        filename <- file.path("coverage", "cobertura.xml")
        res <- covr::to_cobertura(cov = cov, filename = filename)
        print(res)
        if (!file.exists(filename)) {
            stop("File \"", filename, "\" not found!")
        }
    }

    print("End script...")

    if (useCiOptions == 1) {
        # Restore options
        options(beforeOptions)
    }

    invisible(NULL)
}
