# This file contains instructions to initialize
#  the project to your needs with git and R commands.

# You must have done steps describe in README.md before follow this file!

# WARNING: for the rest of the file:
#          - when it says "delete" a file or a folder,
#            this means "Delete from git". For that use the right mouse click
#            to access the TortoiseGit submenu and select "Delete..."
#            on the file(s) to remove.
#          - when it says "rename" a file or a folder,
#            this means "Move from git". For that use the right mouse click
#            to access the TortoiseGit submenu and select "Rename..."
#            on the file(s) to rename.

# This file use commands from devtools package, you can find
# is doc at: https://devtools.r-lib.org/index.html

#####################
### Prerequisites ###
#####################

# - R (https://cloud.r-project.org/)
# - on Windows, RTools (https://cloud.r-project.org/)
# Optional:
# - an IDE for R development:
#   - RStudio (https://posit.co/download/rstudio-desktop/)
#   - VSCode
#   - ...

# Install require R packages
defaultRepo <- "https://cloud.r-project.org"
utils::install.packages(pkgs = "usethis", repos = defaultRepo)
utils::install.packages(pkgs = "testthat", repos = defaultRepo)
utils::install.packages(pkgs = "pkgdown", repos = defaultRepo)
utils::install.packages(pkgs = "spelling", repos = defaultRepo)
utils::install.packages(pkgs = "covr", repos = defaultRepo)
utils::install.packages(pkgs = "devtools", repos = defaultRepo)

# Optional:
#   If you want to generate a pdf manual for your package,
#    then consider these steps:
#     The pdf manual generator will only work if:
#     - is run from RStudio
#     - environment variable RSTUDIO_PANDOC exists
#       and contains the path returned by `rmarkdown:::find_pandoc()` in RStudio
#     - pandoc is manually installed
#     And require these commands to be run:
utils::install.packages(pkgs = "tinytex", repos = defaultRepo)
tinytex::install_tinytex()  # install TinyTeX
#     At this step reboot computer or log out and log in again to Windows session
#      (in order to have the tinytex binaries to the PATH)
#     To solve error "Failed to build manual" with devtools::build_manual()
#      Solution from:
# https://stackoverflow.com/questions/69847540/devtoolsbuild-manual-error-failed-to-build-manual
tinytex::tlmgr_install("makeindex")

####################

# The working directory must be the base folder of your project
#  (the folder you clone, the one with the .git folder)
# You can check which is the current working directory with this:
getwd()
# For the next steps this directory must be the root directory of your package,
#  so adjust it with next command:
#  (don't change the command here, use the R terminal with the right path)
setwd("path/to/the/root/folder/of/the/package")

####################
### License      ###
####################

# If your project dependencies imply nothing to your license
#  (see this wiki for more details: https://sites.inra.fr/site/urep/Wiki%20UREP/Licence.aspx)
#  then download these prefilled files with:
utils::download.file(
    url = "https://forgemia.inra.fr/urep/dev_utils/inrae_urep_software_license/-/raw/main/LICENSE.md?inline=false",
    destfile = "./LICENSE.md",
    method = "auto")
utils::download.file(
    url = paste0("https://forgemia.inra.fr/urep/dev_utils/inrae_urep_software_license/",
                 "-/raw/main/LICENSE_forRPackage?inline=false"),
    destfile = "./LICENSE",
    method = "auto")

# Else download another prefilled license file from project
#  https://forgemia.inra.fr/urep/dev_utils/inrae_urep_software_license
#  or find the template for the required license on Internet
#  and rename the file "LICENSE"

####################
### Project name ###
####################

# Choose the name of your project, this must:
# - Contain only ASCII letters, numbers, and '.'
# - Have at least two characters
# - Start with a letter
# - Not end with '.'

# Edit these files, and change occurrences of "myPackageName"
#  by your chosen name:
#  - file DESCRIPTION
#  - file tests/testthat.R
#  - file .gitlab-ci.yml
# Rename these files, and change occurrences of "myPackageName"
#  by your chosen name:
#  - file myPackageName.Rproj
# Edit file DESCRIPTION and adjust:
# - path to Gitlab pages in URL
# - and BugReports URL
# Edit file _pkgdown.yml and adjust:
# - url: replace "dev_utils/gitlab-project-templates/r-package-project-template"
#        with the gitlab path to your project
#        (the part after "urep")
# - navbar href: with the path to your project on Gitlab
# - home links: modify or add links related to the project
# - reference: modify the first two groups, remove one or add some...
#              The Internal group must not be changed!
# Edit file NEWS.md:
# - adjust url with your project path
# - replace "myPackageName" by your package name
# - when you finalize your first release:
#   replace "**To be completed with release date**" with the current date

####################
### Add code     ###
####################

# The code of your package must be placed in the "R" folder
# You can manually a a file in that folder, or use these commands
# for that (adjust the name for the new file instead of myExampleFunction):

usethis::use_r(name = "myExampleFunction", open = FALSE)
usethis::use_test(name = "myExampleFunction", open = FALSE)

# The provided R/myExampleFunction.R and tests/testthat/test-myExampleFunction.R
#  show an examples for:
#  - an exported function
#  - an internal function
#  - examples with files input and output
#  - tests with files input, output and reference
#  - an example of a vignette
# Look at these files to find out about them, then you can delete them.
# Delete these files too:
# - file inst/examplesdata/exampleData.csv
# - file tests/testthat/testinputs/inputData.csv
# - file tests/testthat/testreferences/dataRef.csv
# - file vignettes/anExampleVignette.Rmd

# Some tips:
# - It is prohibited in a package to use "source" and "library".
#   Instead use "Imports" field of DESCRIPTION file or "Imports" field of function comment.
# - To reference internal function it is required to prefix them with myPackageName:::
# - It is required to comment your functions with roxygen fields
#   - For internal functions add:
#' @keywords Internal
#   - For an exported function add:
#' @export

####################

# If your project has an icon/logo then:
# - Copy this logo to man/figures/logo.png
# - Run this command:
usethis::use_logo("man/figures/logo.png")

####################
### README       ###
####################

# Delete the actual README.md file

# - If you want to print results of R command in the README then you have the possibility to create
#    this README.md from a README.Rmd
#   - delete README.md (not with git for this one) and README-newProject.Rmd
#   - adjust content of README.Rmd for your project (package name, urls, ...)
#   - Uncomment job "README up to date:" in file .gitlab-ci.yml
# - Else
#   - rename README-newProject.md to README.md
#   - delete README.Rmd
#   - adjust content of README.md for your project (package name, urls, ...)
