#' @title A title
#' @description A description
#' @return 4
#' @examples
#' dummy()
#' @export
dummy <- function() {
    return(4)
}
