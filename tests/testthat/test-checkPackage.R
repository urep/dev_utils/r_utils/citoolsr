testthat::test_that("Check is ok", {
    oldWd <- getwd()
    basePath <- testthat::test_path("testinputs", "r-package")
    filepath <- file.path(oldWd, basePath, ".Rbuildignore")

    on.exit(expr = {
        if (file.exists(filepath)) {
            file.remove(filepath)
        }
        file.copy(
            from = file.path(oldWd, basePath, "DESCRIPTION.bak"),
            to = file.path(oldWd, basePath, "DESCRIPTION"),
            overwrite = TRUE)
        if (file.exists("DESCRIPTION.bak")) {
            file.remove("DESCRIPTION.bak")
        }
        setwd(oldWd)
    })

    setwd(basePath)

    if (file.exists("DESCRIPTION.bak")) {
        testthat::expect_true(
            object = file.remove("DESCRIPTION.bak"))
    }
    testthat::expect_true(
        object = file.copy(
            from = file.path(oldWd, basePath, "DESCRIPTION"),
            to = file.path(oldWd, basePath, "DESCRIPTION.bak"),
            overwrite = TRUE))

    testthat::expect_true(
        object = file.copy(
            from = file.path(oldWd, basePath, "Rbuildignore"),
            to = filepath,
            overwrite = TRUE))

    testthat::expect_no_error(
        object = citoolsr::checkPackage(
            useCiOptions = 0,
            errorExpected = 0)
    )
})

testthat::test_that("Check with error expected but no error is ok", {
    oldWd <- getwd()
    basePath <- testthat::test_path("testinputs", "r-package")
    filepath <- file.path(oldWd, basePath, ".Rbuildignore")

    on.exit(expr = {
        if (file.exists(filepath)) {
            file.remove(filepath)
        }
        file.copy(
            from = file.path(oldWd, basePath, "DESCRIPTION.bak"),
            to = file.path(oldWd, basePath, "DESCRIPTION"),
            overwrite = TRUE)
        if (file.exists("DESCRIPTION.bak")) {
            file.remove("DESCRIPTION.bak")
        }
        setwd(oldWd)
    })

    setwd(basePath)

    if (file.exists("DESCRIPTION.bak")) {
        testthat::expect_true(
            object = file.remove("DESCRIPTION.bak"))
    }
    testthat::expect_true(
        object = file.copy(
            from = file.path(oldWd, basePath, "DESCRIPTION"),
            to = file.path(oldWd, basePath, "DESCRIPTION.bak"),
            overwrite = TRUE))

    testthat::expect_true(
        object = file.copy(
            from = file.path(oldWd, basePath, "Rbuildignore"),
            to = filepath,
            overwrite = TRUE))

    testthat::expect_error(
        object = citoolsr::checkPackage(
            useCiOptions = 0,
            errorExpected = 1)
    )
})

testthat::test_that("Check with error expected is ok", {
    oldWd <- getwd()
    basePath <- testthat::test_path("testinputs", "r-package-bad-check")
    filepath <- file.path(oldWd, basePath, ".Rbuildignore")

    on.exit(expr = {
        if (file.exists(filepath)) {
            file.remove(filepath)
        }
        file.copy(
            from = file.path(oldWd, basePath, "DESCRIPTION.bak"),
            to = file.path(oldWd, basePath, "DESCRIPTION"),
            overwrite = TRUE)
        if (file.exists("DESCRIPTION.bak")) {
            file.remove("DESCRIPTION.bak")
        }
        setwd(oldWd)
    })

    setwd(basePath)

    if (file.exists("DESCRIPTION.bak")) {
        testthat::expect_true(
            object = file.remove("DESCRIPTION.bak"))
    }
    testthat::expect_true(
        object = file.copy(
            from = file.path(oldWd, basePath, "DESCRIPTION"),
            to = file.path(oldWd, basePath, "DESCRIPTION.bak"),
            overwrite = TRUE))

    testthat::expect_true(
        object = file.copy(
            from = file.path(oldWd, basePath, "Rbuildignore"),
            to = filepath,
            overwrite = TRUE))

    testthat::expect_no_error(
        object = citoolsr::checkPackage(
            useCiOptions = 0,
            errorExpected = 1)
    )
})
