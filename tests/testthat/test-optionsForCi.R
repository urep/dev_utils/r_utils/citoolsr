testthat::test_that("No error", {
    testthat::expect_no_error(
        object = {
            beforeOptions <- citoolsr::setOptionsForCi()
            # Restore options
            options(beforeOptions)
        }
    )
})

testthat::test_that("Warning as error ok", {
    beforeOptions <- citoolsr::setOptionsForCi()

    testthat::expect_error(
        object = warning("A test warning which must be converted to error")
    )

    # Restore options
    options(beforeOptions)
})
